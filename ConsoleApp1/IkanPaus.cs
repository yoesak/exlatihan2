﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyNamespace
{
    public class IkanPaus : Ikan
    {
        public IkanPaus()
        {
            JumlahSel = 1000;
        }

        public override string BerkembangBiak()
        {
            return "Beranak";
        }
    }
}
