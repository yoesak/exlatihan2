﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Rectangle<T1, T2>
    {
        public Rectangle(T1 panjang, T2 lebar)
        {
            Panjang = panjang;
            Lebar = lebar;
        }

        public T1 Panjang { get; private set; }
        public T2 Lebar { get; private set; }

        public String TampilkanTypeData1()
        {
            return typeof(T1).ToString();
        }
    }
}
