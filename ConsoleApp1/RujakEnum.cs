﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class RujakEnum : IEnumerator<Buah>
    {
        private Int16 _position;
        private Buah[] _rujak;

        public RujakEnum(Buah[] rujak)
        {
            _position = -1;
            _rujak = new Buah[rujak.Length];
            for(var i = 0; i < rujak.Length; i++)
            {
                _rujak[i] = rujak[i];
            }
        }

        public Buah Current => _rujak[_position];

        object IEnumerator.Current => throw new NotImplementedException();

        public void Dispose()
        {
   
        }

        public bool MoveNext()
        {
            _position++;

            return (_position < _rujak.Length);
        }

        public void Reset()
        {
            _position = -1;
        }
    }
}
