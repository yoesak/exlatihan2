﻿using System;
using MyNamespace;
using System.Linq;

namespace ConsoleApp1
{
    
    class Program
    {
        
        static void Main(string[] args)
        {
            /*
            MahlukHidup mahlukHidup = new MahlukHidup(1);
            IkanPaus ikanPaus = new IkanPaus();
            Ikan ikanMas = new Ikan();
            Robot robotCR7 = new Robot();

            Console.WriteLine("Jumlah Sel Ikan Paus {0}", ikanPaus.JumlahSel);
            Console.WriteLine("Cara Berkembang Biak Ikan Paus {0}", ikanPaus.BerkembangBiak());
            Console.WriteLine("Cara Berjalan Ikan Paus {0}", TampilkanCaraBerjalan(ikanPaus));

            Console.WriteLine("Jumlah Sel Ikan Mas {0}", ikanMas.JumlahSel);
            Console.WriteLine("Cara Berkembang Biak Ikan Mas {0}", ikanMas.BerkembangBiak());
            Console.WriteLine("Cara Berjalan Ikan Mas {0}", TampilkanCaraBerjalan(ikanMas));

            Console.WriteLine("Cara Berjalan Robot {0}", TampilkanCaraBerjalan(robotCR7));
            */

            //Rectangle<String, Int32> rect = new Rectangle<String, Int32>("10", 20);

            //Console.WriteLine("Tipe Data = {0}", rect.TampilkanTypeData1());

            //Rectangle<Int32, Int32> rect2 = new Rectangle<Int32, Int32>(10, 20);

            //Console.WriteLine("Tipe Data = {0}", rect2.TampilkanTypeData1());

            var buahs = new Rujak(new Buah[]
            {
                new Buah
                {
                    Nama = "Jambu", 
                    Warna = "Merah"
                },
                new Buah
                {
                    Nama = "Jeruk",
                    Warna = "Kuning"
                }, 
                new Buah
                {
                    Nama = "Salak",
                    Warna = "Coklat"
                },
                new Buah
                {
                    Nama = "Kesemek",
                    Warna = "Kuning"
                },
                new Buah
                {
                    Nama = "Mangga Busuk",
                    Warna = "Coklat Hitam"
                }
            });

            var buahAwalanJ = from b in buahs
                              where b.Nama.EndsWith("k")
                              select b;

            foreach(var buah in buahAwalanJ)
            {
                Console.WriteLine("Yaitu {0}", buah.Warna);
            }
        }

        static String TampilkanCaraBerjalan(IBerjalan oby)
        {
            return oby.CaraBerjalan();
        }
    }
}