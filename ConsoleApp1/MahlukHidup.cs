﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyNamespace
{
    public class MahlukHidup
    {
        public MahlukHidup(Int64 jumlahSel)
        {
            this.JumlahSel = jumlahSel;
        }

        public virtual String BerkembangBiak()
        {
            return "";
        }

        public Int64 JumlahSel { get; protected set; }
    }
}
