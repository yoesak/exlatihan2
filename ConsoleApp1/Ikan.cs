﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyNamespace
{
    public class Ikan : MahlukHidup, IBerjalan
    {
        public Ikan() : base(100)
        {

        }

        public override String BerkembangBiak()
        {
            return "Bertelur";
        }

        public String CaraBerjalan()
        {
            return "Berenang";
        }
    }
}
