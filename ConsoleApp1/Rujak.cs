﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Rujak : IEnumerable<Buah>
    {
        private Buah[] _rujak;

        public Rujak(Buah[] rujak)
        {
            _rujak = new Buah[rujak.Length];
            for(Int16 i=0; i < rujak.Length; i++)
            {
                _rujak[i] = rujak[i];
            }
        }

        public IEnumerator<Buah> GetEnumerator()
        {
            return new RujakEnum(_rujak);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return null;
        }
    }
}
